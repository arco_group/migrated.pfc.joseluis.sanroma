// -*- coding:utf-8; tab-width:4; mode:cpp -*-
/* compile with: gcc -g -Wall listDomains.c -o listDom -lvirt */

#include <stdio.h>
#include <stdlib.h>
#include <libvirt/libvirt.h>

int main(int argc, char *argv[]){

  virConnectPtr conn;
  int i;
  int numDomains;
  int *activeDomains;


  conn = virConnectOpen("qemu:///system");
  if (conn == NULL) {
    fprintf(stderr, "Failed to open connection to qemu:///system\n");
    return 1;
  }
  
  numDomains = virConnectNumOfDomains(conn);

  activeDomains = (int*)malloc(sizeof(int) * numDomains);
  numDomains = virConnectListDomains(conn, activeDomains, numDomains);

  printf("Active domain IDs:\n");
  for (i = 0 ; i < numDomains ; i++) {
    printf("  %d\n", activeDomains[i]);
  }
  free(activeDomains);

  virConnectClose(conn);

  return 0;
}
