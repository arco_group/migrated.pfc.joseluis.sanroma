#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

qemu-img create -f qcow2 debian-ppc.qcow2 5G

wget http://cdimage.debian.org/debian-cd/7.1.0/powerpc/iso-cd/debian-7.1.0-powerpc-netinst.iso

qemu-system-ppc -hda debian-ppc.qcow2 -boot d -cdrom debian-7.1.0-powerpc-netinst.iso

qemu-system-ppc -hda debian-ppc.qcow2
