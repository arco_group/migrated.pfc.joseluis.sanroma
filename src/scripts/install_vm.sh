#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

export LIBVIRT_DEFAULT_URI=qemu:///system

names=(phobos deimos metis adrastea amaltea tebe)
MAC0="00:AA:00:00:00:13"
MAC1="00:AA:00:00:00:14"
MAC2="00:AA:00:00:00:15"
MAC3="00:AA:00:00:00:16"
MAC4="00:AA:00:00:00:17"
MAC5="00:AA:00:00:00:18"

#python -m SimpleHTTPServer 15000 &
#pushd ../Proyecto/src/scripts/; python -m SimpleHTTPServer; popd



#url=https://arco.esi.uclm.es/~joseluis.sanroma/preseed.cfg"

install_i386(){
    echo i386

    name=bully

    virt-install \
        --debug \
        --name=$name-i386 \
        --ram=512 \
		--arch=i386 \
        --disk ./$name-i386.qcow2,format=qcow2,size=5,sparse=true \
		--location=http://ftp.debian.org/debian/dists/stable/main/installer-i386 \
        --network=network=icebuilder,mac=00:AA:00:00:00:11 \
		--extra-args="\
        auto=true priority=critical vga=normal hostname=$name \
        url=http://arco.esi.uclm.es/~joseluis.sanroma/preseed.cfg"
}


# http://d-i.debian.org/daily-images/amd64
# http://d-i.debian.org/daily-images/i386
#http://ftp.us.debian.org/debian/dists/wheezy/main/installer-i386 \
#--cdrom=/home/joseluis.sanroma/isos/debian-testing-i386-CD-1.iso \

install_amd64(){
    echo amd64

    name=rigodon

    virt-install \
        --debug \
        --name=$name-amd64 \
        --ram=512 \
		--arch=x86_64 \
        --disk ./$name-amd64.qcow2,format=qcow2,size=5,sparse=true \
		--location=http://ftp.debian.org/debian/dists/stable/main/installer-amd64 \
        --network=network=icebuilder,mac=00:AA:00:00:00:12 \
		--extra-args="\
        auto=true priority=critical vga=normal hostname=$name \
        url=http://arco.esi.uclm.es/~joseluis.sanroma/preseed.cfg"
}
install_hal9000(){
    echo amd64

    name=hal9000

    virt-install \
        --debug \
        --name=$name \
        --ram=512 \
		--arch=x86_64 \
        --disk ./$name-amd64.qcow2,format=qcow2,size=5,sparse=true \
		--location=http://ftp.debian.org/debian/dists/stable/main/installer-amd64 \
        --network=network=icebuilder,mac=00:AA:00:FF:FF:FF \
		--extra-args="\
        auto=true priority=critical vga=normal hostname=$name \
        url=http://arco.esi.uclm.es/~joseluis.sanroma/preseed-repo.cfg"
}

install_skynet(){
    echo amd64

    name=skynet

    virt-install \
        --debug \
        --name=$name \
        --ram=512 \
		--arch=x86_64 \
        --disk ./$name-amd64.qcow2,format=qcow2,size=5,sparse=true \
		--location=http://ftp.debian.org/debian/dists/stable/main/installer-amd64 \
        --network=network=icebuilder,mac=00:AA:00:FF:FF:FE \
		--extra-args="\
        auto=true priority=critical vga=normal hostname=$name \
        url=http://arco.esi.uclm.es/~joseluis.sanroma/preseed-repo.cfg"
}

create_all(){

    install_amd64 & install_i386
	# install_amd64 ${names[0]} $MAC0 &
	# install_amd64 ${names[1]} $MAC1 &
	# install_amd64 ${names[2]} $MAC2 &
	# install_i386 ${names[3]} $MAC3 &
	# install_i386 ${names[4]} $MAC4 &
	# install_i386 ${names[5]} $MAC5 &

    wait
}

choice(){
    if [ -z $1 ]
    then
        echo todas
        create_all

    else
        echo "sel" $1
        install_$1
    fi

}

choice $1
virt-manager

exit 0
