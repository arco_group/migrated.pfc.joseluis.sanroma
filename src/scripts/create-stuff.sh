#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

NET="xml/net-icebuilder.xml"
POOL="xml/pool-icebuilder.xml"
DIR="../virtualMachines"
DIR_VM="virtualMachines"


create_network(){
    virsh net-define $NET
    virsh net-start icebuilder
    virsh net-autostart icebuilder
}

create_pool(){
    virsh pool-define $POOL
    virsh pool-start icebuilder
    virsh pool-autostart icebuilder
}


sed 's/user/$USER/' xml/pool-icebuilder.conf >> xml/pool-icebuilder.xml
mkdir -p $HOME/.icebuilder
create_network
create_pool
