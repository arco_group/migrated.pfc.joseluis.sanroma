#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

#set timeout -1
 
HOST="192.168.122.113"
USER="arco"
PASS="arco"
PASS_R="clarinetearco"
PACKAGE=gentoo


#sudo virsh snapshot-create-as bully-i386-clone sanp1

#La maquina virtual estÃ¡ en espaÃ±ol, cuando pregunta si instalar los paquetes hay que poner una 'S', si se instala en ingles se pone una 'Y'
#TODO cambiar dpkg-buildpackage por debuild
VAR=$(expect -c "
    
    exp_internal 1

    spawn ssh -o StrictHostKeyChecking=no $USER@$HOST  
    match_max 100000

    expect \"*?assword:*\"
    send -- \"$PASS\r\"
    send -- \"\r\"

    expect \"*?$\"
    send -- \"cd /tmp\r\"

    send -- \"mkdir -p $PACKAGE\r\"
    send -- \"cd $PACKAGE\r\"
    send -- \"apt-get source $PACKAGE\r\"

    set timeout -1
    send -- \"echo Ahora voy a compilar\r\"
    send -- \"cd $PACKAGE-*\r\"
    send -- \"debuild -us -uc\r\"       
    send -- \"exit\r\"

    expect eof
    ")
echo "==============="
echo "$VAR"
