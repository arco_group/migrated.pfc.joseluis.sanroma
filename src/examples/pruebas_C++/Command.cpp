// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "Command.h"


//SimpleCommand
SimpleCommand::SimpleCommand(string a, string o, string n){
  arch=a;
  op=o;
  name=n;
}

void
SimpleCommand::execute(){
  cout << "Simple: " << arch << " " << op  << " " << name << endl;
}
//End SimpleCommand

SimpleCommand::SimpleCommand(string a, string p){
  arch=a;
  package=p;  
}

void
SimpleCommand::execute(){
  cout << "arch: " << arch << "pacakge: " << package <<  endl;
}


//Build

Build::Build(){
  
}


Build::Build(string vMachine, string packageName, string snapshotName){
  arch=vMachine;
  name=packageName;
  snapshot=snapshotName;
  //  commands=com;
  add(new SimpleCommand(arch, name, snapshot));
  add(new SimpleCommand(arch, name, snapshot));
  add(new SimpleCommand(arch, name, snapshot));
  add(new SimpleCommand(arch, name, snapshot));
  add(new SimpleCommand(arch, name, snapshot));
}


void Build::add(Command* c){
  commands.push_back(c);
}

void Build::execute(){

  vector<Command* >:: iterator it =commands.begin();
  while (it != commands.end()){
   (*it++)->execute();
  }    
};
