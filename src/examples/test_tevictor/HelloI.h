// -*- mode: C++; coding: utf-8 -*-

#ifndef __HelloI_h__
#define __HelloI_h__

#include <Freeze/Freeze.h>
#include <IceUtil/IceUtil.h>

#include <Hello.h>


namespace UCLM {

  
  class HelloPersistentI : virtual public HelloPersistent,
                           public IceUtil::AbstractMutexI<IceUtil::Mutex> {
    friend class HelloInitializer;
  public:
    HelloPersistentI();
    virtual void puts(const ::std::string&,
                      const Ice::Current&);
  // private:
  //   int useCount;
  };


typedef IceUtil::Handle<HelloPersistentI> HelloPersistentIPtr;
  

  class HelloFactory : virtual public Ice::ObjectFactory {
  public:
    virtual Ice::ObjectPtr create(const ::std::string&);
    virtual void destroy();
  };


  
  class HelloInitializer : virtual public Freeze::ServantInitializer {
  public:
    virtual void initialize(const Ice::ObjectAdapterPtr& oa,
                            const Ice::Identity& ident,
                            const std::string& str,
                            const Ice::ObjectPtr& obj);    
  };

}

#endif
