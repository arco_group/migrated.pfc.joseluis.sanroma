// -*- coding:utf-8; tab-width:4; mode:cpp -*-

#include <CommandTypeI.h>

namespace FileOperations{
  bool is_empty(std::ifstream& pFile){
    return pFile.peek() == std::ifstream::traits_type::eof();
  }
};


Freeze::TransactionalEvictorPtr CommandType::CompositeCommandI::compositeEvictor;
Freeze::TransactionalEvictorPtr CommandType::CompositeInitializer::initializeEvictor;


//ConcreteCommand

CommandType::ConcreteCommandI::ConcreteCommandI(){
}


void
CommandType::ConcreteCommandI::execute(const Ice::Current&){
}

// void
// CommandType::ConcreteCommandI::destroy(const Ice::Current&){
// }


//SimpleCommand

CommandType::SimpleCommandI::SimpleCommandI(){
}


CommandType::SimpleCommandI::SimpleCommandI(const DebBuilder::Package& p, const std::string& op,
					    const std::string& snapshot){

  debPackage=p;
  operation=op;
  snapshotName=snapshot;

}



// CommandType::SimpleCommandI::SimpleCommandI(std::string a, std::string op,
//                                             std::string sn, std::string pname){

//   debPackage.architecture=a;
//   operation=op;
//   snapshotName=sn;
//   packageName=pname;

// }

void
CommandType::SimpleCommandI::execute(const Ice::Current& current){
  std::cout << "SimpleCommandI: " << debPackage.architecture << " " << operation
            <<" " << debPackage.name << " " << snapshotName <<std::endl;

  std::string command("./../scripts/builder.sh ");
  command.append(debPackage.architecture+" ");
  command.append(operation+" ");
  command.append(debPackage.name+" ");
  if (snapshotName!=""){
    command.append(snapshotName+"-"+debPackage.architecture+" ");
  }

  sleep(5);

  //  std::cout << command << std::endl;

  //system(command.c_str());

}


//CompositeCommand
CommandType::CompositeCommandI::CompositeCommandI(){
}



CommandType::CompositeCommandI::CompositeCommandI(const ::DebBuilder::Package& p)
  :_destroy(true){
  Ice::Current current;
  debPackage.name=p.name;
  debPackage.architecture=p.architecture;
  debPackage.version=p.version;
  debPackage.state=p.state;


  add(new SimpleCommandI(debPackage,"turnOff", ""),current);
  add(new SimpleCommandI(debPackage,"deleteSnapshot", "snapshot"),current);
  add(new SimpleCommandI(debPackage,"revertSnapshot", "snapshot"),current);
  add(new SimpleCommandI(debPackage,"buildPackage", ""),current);
  add(new SimpleCommandI(debPackage,"sourcePackage", ""),current);
  add(new SimpleCommandI(debPackage,"buildDependencies", ""),current);
  add(new SimpleCommandI(debPackage,"createSnapshot", "snapshot"),current);
  add(new SimpleCommandI(debPackage,"upgrade",""),current);
  add(new SimpleCommandI(debPackage,"update",""),current);
  add(new SimpleCommandI(debPackage,"start", ""),current);

  // std::cout << "Package name: " << debPackage.name << std::endl;


}

CommandType::CompositeCommandI::~CompositeCommandI(){

}

// void
// CommandType::CompositeCommandI::setPackage(const ::DebBuilder::Package& p,
//                                            const ::Ice::Current& current){

//   IceUtil::Mutex::Lock lock(compositeMutex);

//   debPackage=p;
//   debPackage.state=building;
//   std::cout << "Package name: " << this->debPackage.name << std::endl;
//   std::cout << "Package arch: " << this->debPackage.arch << std::endl;
//   std::cout << "Package state: " << this->debPackage.state << std::endl;


//   std::cout << "AQUI" << std::endl;
//   // std::cout << "Identity: " << id.name << std::endl;
//   CompositeCommandPtr cc=new CompositeCommandI(debPackage);
//   Ice::Identity id;
//   id.name=debPackage.name+"-"+debPackage.version+"_"+debPackage.arch;
//     // IceUtil::generateUUID();
//   cc->parent=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(current.id));
//   CompositeCommandPrx proxy=CompositeCommandPrx::uncheckedCast(compositeEvictor->add(cc,id));


//   composites.push_back(proxy);
//   // add(cc,current);

//   std::cout << "compositesSize: " << composites.size() << std::endl;

//   // execute(current);

// }

void
CommandType::CompositeCommandI::init(const ::DebBuilder::Package& p,
				     const Ice::Current& current){
  IceUtil::Mutex::Lock lock(compositeMutex);
  std::cout << "metdodo init " << std::endl;

  debPackage=p;
  add(new SimpleCommandI(debPackage,"turnOff", ""),current);
  add(new SimpleCommandI(debPackage,"deleteSnapshot", "snapshot"),current);
  add(new SimpleCommandI(debPackage,"revertSnapshot", "snapshot"),current);
  add(new SimpleCommandI(debPackage,"buildPackage", ""),current);
  add(new SimpleCommandI(debPackage,"sourcePackage", ""),current);
  add(new SimpleCommandI(debPackage,"buildDependencies", ""),current);
  add(new SimpleCommandI(debPackage,"createSnapshot", "snapshot"),current);
  add(new SimpleCommandI(debPackage,"upgrade",""),current);
  add(new SimpleCommandI(debPackage,"update",""),current);
  add(new SimpleCommandI(debPackage,"start", ""),current);
}



//::DebBuilder::PackageList
::Ice::Int
CommandType::CompositeCommandI::
generateDependencyTree(const ::DebBuilder::Package& p,
		       ::Ice::Int depth,
		       const Ice::Current& current){

  int depthTemp;
  CompositeCommandPrx currentPrx, buildDepPrx;
  currentPrx=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(current.id));

  PackageList dependencies {};
  dependencies=currentPrx->generateDependencies(p);

  if (dependencies.empty()){
    return depth;
  }
  else{
    while(!dependencies.empty()){

      buildDepPrx=currentPrx->createComposite(dependencies.front());
      depthTemp=buildDepPrx->generateDependencyTree(dependencies.front(),depth+1);
      dependencies.pop_front();

    }
    if (depthTemp>depth){
      depth=depthTemp;
    }
  }

  return depth;
}



::DebBuilder::PackageList
CommandType::CompositeCommandI::generateDependencies(const ::DebBuilder::Package& p,
						     const Ice::Current& current){


  std::string path="../scripts/";
  std::string script="build_depends_list.sh";
  std::string command;
  command.append(path+script+" "+p.name);
  // command.append(path);
  // command.append(script+" ");
  // command.append(p.name);
  system(command.c_str());

  std::ifstream in(p.name);

  ::DebBuilder::PackageList tempDependencies {};

  if(!FileOperations::is_empty(in)){
    ::DebBuilder::Package dependency {};
    dependency.architecture = p.architecture;
    dependency.version = "0";
    dependency.state = needsBuild;
    // while(!fe.eof()){
    //   getline(fe,dependency.name);
    //   tempDependencies.push_back(dependency);
    // }
    // tempDependencies.pop_back();
    while (true){
      in >> dependency.name;

      if(in.eof())
	break;

      std::cout << dependency.name << std::endl;
      tempDependencies.push_back(dependency);

    }
  }

  // tempDependencies.pop_back();
  return tempDependencies;
}


bool
CommandType::CompositeCommandI::buildable(const ::DebBuilder::Package& p,
					  const Ice::Current& current){


  std::string path="../scripts/";
  std::string script="check-builddepends.sh";
  std::string command;

  command.append(path+script+" "+p.architecture+" "+p.name);
  // command.append(path);
  // command.append(p.arch+" ");
  // command.append(p.name+" ");
  // command.append(p.distribution+" ");
  system(command.c_str());

  std::string file=p.name + ".yaml";
  YAML::Node node=YAML::LoadFile(file);

  std::string st=node["status"].as<std::string>();

  if(st=="ok")
    return true;
  else
    return false;
}


void
CommandType::CompositeCommandI::add(const ::CommandType::ConcreteCommandPtr& c,
                                   const Ice::Current& current){

  // std::cout << "P=" << hola << std::endl;
  // c->id.name=IceUtil::generateUUID();
  // c->id.name=c->debPackage.name+"-"+c->debPackage.version;

  std::string type=c->ice_id();
  // std::cout << "TIPO" << type << std::endl;
  // type=CompositeCommand::ice_staticId();


  // if (type ==CompositeCommand::ice_staticId()){

  //   std::cout << "\nAdding CompositeCommand" << std::endl;
  //   // std::cout << "ID: " << c->id.name << std::endl;
  //   c->parents=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(current.id));
  //   // std::cout << "current: "<< current.id.name << std::endl;
  //   // CompositeCommandPrx proxy=CompositeCommandPrx::uncheckedCast(compositeEvictor->add(c,c->id));
  //   // compositeEvictor->add(c,c->id);
  // }

  commands.push_back(c);
  // Freeze::EvictorIteratorPtr p=compositeEvictor->getIterator("",20);
  // while(p->hasNext()){
  //   Ice::Identity ident=p->next();
  //   std::cout << "INITIALIZE i  dentName: " << ident.name << std::endl;
  // }
}


// // ::std::string
// // CommandType::CompositeCommandI::getPackage(const Ice::Current& current){
// //   // return ::DebBuilder::Package();
// //   // std::cout << commands.size() << std::endl;
// //   std::cout << composites.size() << std::endl;
// //   return commands.back()->debPackage.name;
// // }


// // void
// // CommandType::CompositeCommandI::build(const ::DebBuilder::Package& p,
// // 					    const Ice::Current& current){


// //   // std::cout << "Identity: " << id.name << std::endl;
// //   std::cout << "IdentityCURRENT: " << current.id.name << std::endl;
// //   CompositeCommandPrx prx, compositePrx;
// //   prx=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(current.id));

// //   CompositeCommandPtr c=new CompositeCommandI(p);



// //   // prx->setPackage(p);

// //   DebBuilder::Package p3;
// //   p3.name="gentoo";
// //   p3.arch="amd64";
// //   p3.version="1.0";
// //   p3.state=needsBuild;
// //   // prx->setPackage(p3);
// //   // prx->execute();


// //   Freeze::EvictorIteratorPtr ei=compositeEvictor->getIterator("",20);
// //   while(ei->hasNext()){
// //     Ice::Identity ident=ei->next();
// //     std::cout << "INITIALIZE IT identName: " << ident.name << std::endl;
// //   }

// //   prx->buildDFS();


// //   // prx->destroy();

// // }


// void
// CommandType::CompositeCommandI::buildDFS(const Ice::Current& current){

//   IceUtil::Mutex::Lock lock(compositeMutex);


//   CompositeCommandPrx prx;
//   prx=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(current.id));

//   std::cout << "buildDFS" << std::endl;
//   std::cout << "composites: " << composites.size() << std::endl;

//   if(composites.empty()){
//     prx->execute();
//     // prx->destroy();

//   }
//   else{

//     std::cout << "Hay mÃ¡s de uno" << std::endl;
//     while(!composites.empty()){
//       composites.back()->buildDFS();
//       composites.back()->destroy();
//       if(composites.empty()){
// 	std::cout << "EMPTY" << std::endl;
//       }
//       std::cout << "COMPSOITES---->" << composites.size() << std::endl;
//       prx->removeComposite();
//     }

//     // prx->buildDFS();
//   }

// }

// ::CommandType::CompositeCommandPrx
// CommandType::CompositeCommandI::getComposites(const Ice::Current& current){
//   CompositeCommandPrx proxy=composites.back();


// }



void
CommandType::CompositeCommandI::execute(const Ice::Current& current){

  IceUtil::Mutex::Lock lock(compositeMutex);

  // std::cout << "CompositeCommandI: " << debPackage.name << std::endl;
  // CommandSeq::iterator it = commands.begin();

  // Ice::Identity id=current.id;
  // std::cout << "CompositeEXECUTE current" << id.name << std::endl;

  // std::string type=commands.back()->ice_id();
  // std::cout << "type " << type << std::endl;

  // // if(type==CompositeCommand::ice_staticId()){
  // //   std::cout << "composite" << std::endl;
  // //   // std::cout << commands.back()->id.name << std::endl;
  // //   CompositeCommandPrx cPrx;
  // //   cPrx=CompositeCommandPrx::checkedCast(current.adapter->createProxy(commands.back()->id));
  // //   cPrx->execute();
  // //   commands.pop_back();
  // //   cPrx->destroy();

  // // }
  // // else{

  //   while(!commands.empty()){

  //     commands.back()->execute();
  //     commands.pop_back();
  //   }
  // }
}


::CommandType::CompositeCommandPrx
CommandType::CompositeCommandI::createComposite(const ::DebBuilder::Package& p,
						const Ice::Current& current){
  IceUtil::Mutex::Lock lock(compositeMutex);

  Ice::Identity id;
  id.name=p.name+"-"+p.architecture;

  if (compositeEvictor->hasObject(id)){
      CompositeCommandPrx compositePrx;
      compositePrx=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(id));
      // buildDepends.push_back(packagePrx);
      return compositePrx;
  }

  else{
    CompositeCommandPrx currentPrx=CompositeCommandPrx::uncheckedCast(current.adapter->createProxy(current.id));
    CompositeCommandPtr composite=new CompositeCommandI(p);
    composite->parents.push_back(currentPrx);
    CompositeCommandPrx proxy=CompositeCommandPrx::uncheckedCast(compositeEvictor->add(composite,id));
    buildDepends.push_back(proxy);
    // composite->parents.push_back(currentPrx);
    return proxy;
  }
}

::CommandType::CompositeSeq
CommandType::CompositeCommandI::reverseDFS(::Ice::Int depth,
					   ::CommandType::CompositeSeq& compositePackages,
					   const ::Ice::Current& current){


  if (depth==0){
      compositePackages.push_back(this);
  }

  else{
    CompositePrxList::iterator it=buildDepends.begin();
    while(it!=buildDepends.end()){
      (*it)->reverseDFS(depth-1, compositePackages);
      it++;
    }
  }

  std::cout << "size reverse: " << compositePackages.size() << std::endl;
  return compositePackages;

}


::CommandType::CompositeSeq
CommandType::CompositeCommandI::getParents(const ::Ice::Current&){

}


// void
// CommandType::CompositeCommandI::destroy(const Ice::Current& current){

//   IceUtil::Mutex::Lock lock(compositeMutex);

//   // std::cout << "DESTROYING... ID: " << id.name << std::endl;


//   std::cout << "CURRENT: " << current.id.name << std::cout;

//   try{
//     // parent->removeComposite();
//     compositeEvictor->remove(current.id);
//   } catch (const Ice::NotRegisteredException&){
//     throw Ice::ObjectNotExistException(__FILE__, __LINE__);
//   }
//   std::cout << "Destroyed!" << std::endl;
// }





// void
// CommandType::CompositeCommandI::removeComposite(const Ice::Current& current){
//   IceUtil::Mutex::Lock lock(compositeMutex);
//   composites.pop_back();
//   destroy(current);

// }



//CommandFactory

Ice::ObjectPtr
CommandType::CompositeFactory::create(const std::string& type){
  std::cout << type << std::endl;
  if (type == CompositeCommand::ice_staticId()){
    CompositeCommandPtr cc {new CompositeCommandI};
    return cc;
  }
  else{
    if (type== SimpleCommand::ice_staticId()){
      SimpleCommandPtr sc {new SimpleCommandI};
    return sc;

    }

  }
  // else{
  //   assert(false);
  //   return 0;
  // }
}

void
CommandType::CompositeFactory::destroy(){
}


typedef IceUtil::Handle<CommandType::CompositeCommandI> CompositeCommandIPtr;


void
CommandType::CompositeInitializer::initialize(const Ice::ObjectAdapterPtr& oa,
                                              const Ice::Identity& ident,
                                              const std::string& str,
                                              const Ice::ObjectPtr& obj){


  CompositeCommandIPtr ptr = CompositeCommandIPtr::dynamicCast(obj);
  ptr->commands={};



  // std::cout << " Identity:" << ident.name << std::endl;
  std::cout << " Package: " << ptr->debPackage.name << std::endl;
  std::cout << " Package: " << ptr->debPackage.state << std::endl;
  // std::cout << " commandsSize: " << ptr->commands.size() << std::endl;
  // std::cout << "compositesSize: " << ptr->composites.size() << std::endl;


  // ptr->commands.back()->execute();





  // Freeze::EvictorIteratorPtr p=initializeEvictor->getIterator("",20);
  // while(p->hasNext()){
  //   Ice::Identity ident=p->next();
  //   std::cout << "INITIALIZE IT identName: " << ident.name << std::endl;
  // }


}
