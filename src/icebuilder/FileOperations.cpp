// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <FileOperations.h>

using namespace FileOperations;

bool is_empty(std::ifstream& pFile){

  return pFile.peek() == std::ifstream::traits_type::eof();
}
