// -*- coding:utf-8; tab-width:4; mode:cpp -*-

#include <DebBuilderI.h>

// CommandI
void
DebBuilder::CommandI::execute(const Ice::Current& current)
{
}


// Manager
DebBuilder::ManagerI::ManagerI(){

}
DebBuilder::ManagerI::ManagerI(CommandType::CompositeCommandPrx prx){
  compositePrx=prx;

}

void
DebBuilder::ManagerI::build(const ::DebBuilder::Package& p,
                                 const Ice::Current& current){

  compositePrx->init(p);
  PackageList dependencies {};

  int depth;
  depth=compositePrx->generateDependencyTree(p,0);


  // packagesToBuild=compositePrx->reverseDFS(depth, packagesToBuild);

  // std::cout << "PACKAGEsTOBUILD :" << packagesToBuild.size() << std::endl;

  // DebBuilder::PackageList::iterator it=dependencies.begin();

  // CommandType::CompositeCommandPrx dependencyPrx;

  // std::cout << "size :" << dependencies.size() << std::endl;


  // while(it!=dependencies.end()){
  //   std::cout << (*it).name << std::endl;
  //   // dependencyPrx=compositePrx->createComposite(it);
  //   it++;
  // }
}

// Buildability of a given package
  // YAML::Node node=YAML::LoadFile("file.yaml");

  // assert(node.Type() == YAML::NodeType::Map);
  // assert(node.IsMap());  // a shortcut!

  // std::string st;

  // if (node["status"])
  //   {
  //     std::cout << node["status"].as<std::string>() << std::endl;
  //     st=node["status"].as<std::string>();
  //   }

  // std::cout << "ST " << st << std::endl;





// ::DebBuilder::PackageList
// DebBuilder::ManagerI::generateDependencies(const ::DebBuilder::Package& p,
//                                            const Ice::Current& current){
//   std::string path="../scripts/";
//   std::string script="build_depends.sh";
//   std::string command;
//   command.append(path);
//   command.append(script+" ");
//   command.append(p.name);
//   system(command.c_str());

//   std::ifstream fe(p.name);

//   ::DebBuilder::PackageList tempDependencies {};

//   ::DebBuilder::Package dependency {};
//   dependency.arch=p.arch;

//   while(!fe.eof()){
//     getline(fe,dependency.name);
//     tempDependencies.push_back(dependency);
//   }

//   std::cout << tempDependencies.size();
//   return ::DebBuilder::PackageList();
// }