#include <CommandType.ice>

module Work{
  interface Worker{
    void init(CommandType::CompositeCommand p);
    ["freeze:write"]void puts(CommandType::CompositeCommand p);
    ["freeze:write"]void exec(CommandType::CompositeCommand p);
    ["freeze:write"]void upload();

  };
};
