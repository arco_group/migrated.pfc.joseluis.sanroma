// -*- coding:utf-8; tab-width:4; mode:cpp -*-

#ifndef __DebBuilderI_h__
#define __DebBuilderI_h__

#include <IceUtil/IceUtil.h>

#include <CommandType.h>
#include <DebBuilder.h>
#include <fstream>



using namespace CommandType;


namespace DebBuilder
{

  class CommandI : virtual public Command
  {
  public:

    virtual void execute(const Ice::Current&);
  };

  class ManagerI : virtual public Manager
  {

  public:
    ManagerI();
    ManagerI(CommandType::CompositeCommandPrx prx);

    virtual void build(const ::DebBuilder::Package&,
			    const Ice::Current&);
    /* virtual ::DebBuilder::PackageList generateDependencies(const ::DebBuilder::Package&, */
    /*                                                        const Ice::Current&); */


  private:
    CommandType::CompositeCommandPrx compositePrx;
    ::DebBuilder::PackageList dependencies;
    CommandType::CompositeSeq packagesToBuild;

  };



  /* class CompositeI : virtual public Composite */
  /* { */
  /* public: */

  /*   virtual void setPackage(const ::DebBuilder::Package&, const Ice::Current&); */
  /*   virtual void build(const ::DebBuilder::Package&, const Ice::Current&); */
  /*   virtual ::std::string getPackage(const Ice::Current&); */
  /*   virtual void removeComposite(const Ice::Current&); */

  /*   /\* virtual void initPackage(const ::DebBuilder::Package&,const Ice::Current&); *\/ */

  /* }; */
}

#endif
