// -*- coding:utf-8; tab-width:4; mode:cpp -*-


#include <Ice/Ice.h>


// #include <DebBuilder.h>
#include <Worker.h>
#include <WorkerI.h>
// #include <CommandType.h>
// #include <CommandTypeI.h>


using namespace std;
using namespace Ice;
using namespace Work;


class WorkerServer: public Ice::Application{
public:

  int run(int argc, char* argv[]){

    shutdownOnInterrupt();


    Ice::ObjectFactoryPtr factory=new WorkerFactory;

    communicator()->addObjectFactory(factory, WorkerPersistent::Job::ice_staticId());


    Ice::ObjectAdapterPtr adapter=communicator()
            ->createObjectAdapterWithEndpoints("WorkerAdapter", "tcp -p 15888");

    Freeze::TransactionalEvictorPtr workerEvictor=
      Freeze::createTransactionalEvictor(adapter,"db","workerEvictor",
					 Freeze::FacetTypeMap(),
					 new WorkerInitializer());

    adapter->addServantLocator(workerEvictor,"");

    Ice::Identity workerId=communicator()->stringToIdentity("worker");

    WorkerPrx prx;
    prx=Work::WorkerPrx::uncheckedCast(adapter->createProxy(workerId));

    if (workerEvictor->hasObject(workerId)){
      std::cout << workerId.name << " It was already in this worker evictor" << std::endl;
    }
    else{
      Ice::ObjectPrx obj=workerEvictor->add(new WorkerI, workerId);
      std::cout << communicator()->proxyToString(obj) << std::endl;
    }





    adapter->activate();
    communicator()->waitForShutdown();

    return 0;
  }

};



int main(int argc, char *argv[]){
  WorkerServer app;
  return app.main(argc,argv);
  exit(0);
}

// #include <WorkerI.h>
// #include <Ice/Ice.h>


// using namespace std;
// using namespace Ice;
// using namespace Work;


// class WorkerServer: public Ice::Application{
//   int run(int arg, char**argv){

    // shutdownOnInterrupt();

    // Ice::ObjectFactoryPtr factory=new WorkerFactory;

    // communicator()->addObjectFactory(factory, WorkerPersistent::Job::ice_staticId());

    // Ice::ObjectAdapterPtr adapter;
    // adapter=communicator()->createObjectAdapter("WorkerAdapter");

    // Freeze::TransactionalEvictorPtr workerEvictor=
    //   Freeze::createTransactionalEvictor(adapter,"db","workerEvicor",
    //                                      Freeze::FacetTypeMap(),
    //                                      new WorkerInitializer());

    // adapter->addServantLocator(workerEvictor,"");

    // WorkerI::workerEvictor=workerEvictor;
    // adapter->addServantLocator(workerEvictor,"");

    // Ice::Identity workerId=communicator()->stringToIdentity("worker");

    // WorkerPrx prx;

    // prx=WorkerPrx::unchekedCast(adapter->createProxy(workerId));

    // if(workerEvictor->hasObject(workerId)){
    //   std::cout << workerId.name << "It was already in this evictor" << std::endl;
    //   prx->build();
    // }
    // prx->build();



    // adapter->activate();
    // communicator()->waitForShutdown();

//   }

// };


// int main(int argc, char *argv[]){
//   WorkerServer app;
//   return app.main(argc,argv);
// }
