#ifndef FILEOPEARTIONS_H
#define FILEOPEARTIONS_H

#include <fstream>
#include <iostream>
/* #include <unistd.h> */
/* #include <stdlib.h> */

namespace FileOperations{

  bool is_empty(std::ifstream& pFile);

};
#endif
