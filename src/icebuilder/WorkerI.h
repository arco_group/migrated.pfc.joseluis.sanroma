#ifndef __WorkerI_h__
#define __WorkerI_h__

#include <Worker.h>
#include <CommandType.h>
#include <WorkerPersistent.h>
#include <IceUtil/IceUtil.h>
#include <Freeze/Freeze.h>


namespace Work
{

  class WorkerI : virtual public Worker,
		  public WorkerPersistent::Job,
		  public IceUtil::AbstractMutexI<IceUtil::Mutex>
{
  friend class WorkerInitializer;
public:

  virtual void init(const ::CommandType::CompositeCommandPtr&,
		    const Ice::Current&);

  virtual void puts(const CommandType::CompositeCommandPtr& p,
		    const Ice::Current& current);

  virtual void exec(const ::CommandType::CompositeCommandPtr&,
		    const Ice::Current&);

  virtual void upload(const Ice::Current&);
};


class WorkerFactory : public Ice::ObjectFactory
{
public:
  virtual Ice::ObjectPtr create(const std::string&);
  virtual void destroy();
};

class WorkerInitializer : public Freeze::ServantInitializer
{
public:
  virtual void initialize(const Ice::ObjectAdapterPtr&,
			  const Ice::Identity&,
			  const std::string&,
			  const Ice::ObjectPtr&);
};

}

#endif
