#include <WorkerI.h>

void
Work::WorkerI::init(const CommandType::CompositeCommandPtr& p,
                    const Ice::Current& current)
{


}

void
Work::WorkerI::puts(const CommandType::CompositeCommandPtr& p,
		    const Ice::Current& current)
{
  package=p;
}

void
Work::WorkerI::exec(const ::CommandType::CompositeCommandPtr& p, const Ice::Current& current){
  std::cout << "exec" << std::endl;
}

void
Work::WorkerI::upload(const Ice::Current& current)
{
  std::cout << "Package upload" << std::endl;
}


Ice::ObjectPtr
Work::WorkerFactory::create(const std::string& type){
  if (type==Worker::ice_staticId()){
    return new WorkerI;
  }
}

void
Work::WorkerFactory::destroy(){
}



void
Work::WorkerInitializer::initialize(const Ice::ObjectAdapterPtr& oa,
				    const Ice::Identity& ident,
				    const std::string& str,
				    const Ice::ObjectPtr& obj){

}
