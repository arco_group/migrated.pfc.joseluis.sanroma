[["cpp:include:list"]]

module DebBuilder{

  enum StateType{
    needsBuild,
    building,
    uploaded,
    depWait,
    bdUnstallable,
    failed,
    notForUs,
    installed
  };

  enum DistributionType{
    stable,
    testing,
    unstable,
  };

  exception GenericError{
    string reason;
  };
  exception BuildabilityError extends GenericError{};

  struct Package{
    string name;
    string architecture;
    string version;
    StateType state=needsBuild;
    // DistributionType distribution;
  };

  ["cpp:type:std::list<::DebBuilder::Package>"]
  sequence<Package> PackageList;


  interface Command{
    ["freeze:write:required"]void execute();
  };


  interface Manager{
    void build(Package p);
    // PackageList generateDependencies(Package p);
  };

  interface Worker{
    ["freeze:write"]void exec();
    ["freeze:write"]void upload();
    //["freeze:write"]void sign();
  };

};
