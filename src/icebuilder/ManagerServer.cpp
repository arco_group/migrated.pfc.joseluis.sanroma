// -*- coding:utf-8; tab-width:4; mode:cpp -*-

#include <Ice/Ice.h>


// #include <DebBuilder.h>
#include <DebBuilderI.h>
// #include <CommandType.h>
#include <CommandTypeI.h>


using namespace std;
using namespace Ice;
using namespace CommandType;


class ManagerServer: public Ice::Application{
public:

  int run(int argc, char* argv[]){

    shutdownOnInterrupt();


    // Ice::ObjectFactoryPtr factory=new CompositeFactory;
    Ice::ObjectFactoryPtr factory {new CompositeFactory};
    communicator()->addObjectFactory(factory, CompositeCommand::ice_staticId());
    communicator()->addObjectFactory(factory, SimpleCommand::ice_staticId());


    Ice::ObjectAdapterPtr adapter=communicator()
      ->createObjectAdapterWithEndpoints("ManagerAdapter", "tcp -p 13888");




    // Evictor creation
    Freeze::TransactionalEvictorPtr compositeEvictor;
    compositeEvictor=createTransactionalEvictor(adapter, "db","composite",
    						Freeze::FacetTypeMap(),
    						new CompositeInitializer());

    CommandType::CompositeCommandI::compositeEvictor=compositeEvictor;

    adapter->addServantLocator(compositeEvictor, "");


    Ice::Identity compositeId=communicator()->stringToIdentity("MainComposite");
    CommandType::CompositeCommandPrx prx;
    prx=CommandType::CompositeCommandPrx::uncheckedCast(adapter->createProxy(compositeId));



    if(compositeEvictor->hasObject(compositeId)){
      std::cout << compositeId.name << "It was already in this composite evictor" << std::endl;
    }
    else{
      CompositeCommandIPtr cc { new CompositeCommandI };
      Ice::ObjectPrx obj =compositeEvictor->add(cc,compositeId);
      // Ice::ObjectPrx obj =compositeEvictor->add(CompositeCommandPtr cc,compositeId);
      // std::cout << communicator()->proxyToString(obj) << std::endl;
    }
    //Manager creation
    DebBuilder::ManagerPtr manager=new DebBuilder::ManagerI(prx);

    Ice::ObjectPrx managerPrx= adapter->add(manager,communicator()->
					    stringToIdentity("Manager"));

    cout << communicator()->proxyToString(managerPrx) << endl;

    adapter->activate();
    communicator()->waitForShutdown();

    std::cout << "Done !" << endl;

    return 0;
  }

};



int main(int argc, char *argv[]){
  ManagerServer app;
  return app.main(argc,argv);
}
