1. Breve descripción del proyecto 
----------------------------------
   (Se recomienda lectura de anteproyecto en la sección Downloads del
   repositorio para una lectura más detallada):
   https://bitbucket.org/arco_group/pfc.joseluis.sanroma/downloads/anteproyecto.pdf

   Infraestructura distribuida para la construcción de paquetes
   debian. Utilizando el middleware de comunicaciones ICE la idea es contruir un
   sistema distribuido para la construcción de paquetes Debian no oficiales
   para tener los paquetes compatibles con las arquitecturas del entorno de
   trabajo (i386, amd64, arm, etc). Como el entorno de trabajo es limitado,
   se diferencia de otros sistemas como "buildd" en que se utilizan solamente
   los computadores disponibles. Una de las principales características es
   que se podrá congelar la construcción, por ejemplo, si un computador
   necesita apagarse y está construyendo un paquete, esta construcción se
   podrá parar y seguir otro día por donde se dejó. Este proyecto esta
   pensado para el Laboratorio de investigación Arco (ver info adicional),
   pero se hará de forma general para que se pueda utilizar en otros entornos


2. Descripción del árbol de directorios del proyecto
-----------------------------------------------------
   El árbol del proyecto es el siguiente:
   /doc (documentación maquetada en Latex y dividida en carpetas)
        /doc/ante (anteproyecto presentado y aprobado)
        /doc/diagrams (diagramas hechos con dia)
        /doc/manual (manual de instalación de un nodo)
        /doc/memoria (memoria del proyecto, versión en desarrollo constante)
   /src (código fuente)
        /src/xml (configuraciones xml)
   

3. Como obtener el proyecto
----------------------------
   
   Para descargar el respositorio basta con copiar en consola:
   hg clone https://bitbucket.org/arco_group/pfc.joseluis.sanroma
   
   Aunque ya está en la sección de descargar del repositorio, si se desea
   compilar la memoria se necesita el paquete Debian arco-pfc:
   https://bitbucket.org/arco_group/arco-pfc  

   Cuando se cree la primera versión estable se empaquetará todo para Debian
   siguiendo la Debian Policy[7]


4. Requisitos del proyecto 
--------------------------- 

   Este proyecto solamente está probado en una distribución de GNU Debian
   GNU/Linux version SID y testing. No se garantiza que funcione en otra
   distribución de GNU

   Los paquetes que hacen falta para ejecutar (todos en los repos oficiales)
   el proyecto son:
   ZeroC-ice  
   libvirt 
   virtinst 
   virt-viewer 
   virt-manager
   python

   Paquetes no oficiales necesarios para la documentación:
   arco-pfc[6]

   Está todo documentado en los manuales de instalación
   

5. Pasos para realizar la instalación 
--------------------------------------

   5.1. Instalación de un nodo
   ----------------------------

   La instalación de un nodo se puede seguir desde el pdf que lo explica en
   esta dirección:
   https://bitbucket.org/arco_group/pfc.joseluis.sanroma/downloads/node_builder_manual.pdf

   5.2. Ejecución de un nodo 
   -------------------------- 

   Una vez que una o varias máquinas virtuales están instaladas, en el
   directorio src/prueba_packages hay una prueba en python que corresponde
   con la iteración 2 de la memoria, en la que pretende demostrar que el
   sistema construye más de un paquete y está preparado para las sucesivas
   iteraciones. Se recomienda ver el vídeo de demostración[8] para ver el
   proceso


   Para ejecutar la prueba ejecute necesitan 3 terminales:
   El primero para ejecutar el servidor con "make run-server"
   El segundo para ejecutar uno de los clientes "make run-client1"
   El tercero para ejecutar el otro cliente "make run-client2"

   Se recomienda el uso de "terminator", un terminal que permite abrir
   multiples terminales en una ventana.

   Esta prueba se puede ejecutar en varios ordenadores, pero por simpleza se
   recomienda ejecutarlo en uno simulando los dos nodos por el momento. En la
   prueba, como se puede ver en el video de demostración[8] no se paraleliza
   la construcción de los paquetes, la prueba está hecha así para hacer la
   demostración pero pueden construirse dos paquetes a la vez y aprovechar
   concurrencia si su CPU tiene más de un core. Esto se hace editando el
   Makefile y diciéndole que se quiere construir el paquete para las dos
   arquitecturas poniendo "all" en lugar de la arquitectura: 

   ./Client.py "$(shell head -1 proxy.out)" gentoo i386

   ./Client.py "$(shell head -1 proxy.out)" gentoo all

   Así el paquete gentoo se generará para ambas arquitecturas gracias a la
   orden "all". Todo el proceso de la prueba se puede seguir desde la
   terminal donde se ejecute el servidor.



   El script build_package.sh tiene está preparado para ejecutar dos
   máquinas, en las líneas 9 y 10 del script están las IP de las máquinas
   virtuales, si esas IP no corresponden con las de las máquians virtuales
   cambiela para que coincidan.

   La firma de paquete no se incluye porque en cada ordenador puede haber una
   distinta, es responsabilidad de cada uno incorporarla siguiendo la
   iteracion 2 en la documentación. Para añadirlo, crear las claves y editar
   la linea 32 del scrip "build_package.sh" con debuild -kTUCLAVE

   La subida al repositorio tampoco está añadida al script porque requiere de
   la configuración de un repositorio, ya sea con reprepro u otro software
   parecido.
   
   
   

6. Documentación del proyecto
------------------------------
   
   La documentación se encuentra en el directorio
   /doc y hay unos pdf listos donde se explica el paso a paso de la
   instalación de uno de los nodos. Estos pdf están en la sección Downloads
   del repositorio:
   https://bitbucket.org/arco_group/pfc.joseluis.sanroma/downloads
   
   *Memoria: Memoria del proyecto, ya compilada y lista para leer, se
    explican las iteraciones llevadas a cabo hasta el momento y las tomas de
    decisiones. También la metodología. Es el documento donde están también
    los manuales.
    https://bitbucket.org/arco_group/pfc.joseluis.sanroma/downloads/memoria.pdf


   *Anteproyecto: documento presentado y aprobado para la realización de este
    PFC. Dará una visión general de lo que se pretende con este proyecto.
    https://bitbucket.org/arco_group/pfc.joseluis.sanroma/downloads/anteproyecto.pdf
    
   
   *Instalación Nodo: Por la naturaleza del proyecto es necesario instalar y
    configurar un Nodo, esto está explicado en este documento y también en
    los apendices de la memoria.
    https://bitbucket.org/arco_group/pfc.joseluis.sanroma/downloads/node_builder_manual.pdf

    *Blog[2]: En el blog hay numerosas entradas que al fin y al cabo son una
     versión adaptada de la documentación.
   
   

7. Información adicional
-------------------------

   José Luis Sanroma Tato[5] es el único desarrollador de este proyecto, la
   actividad del proyecto se desarrolla dentro del Laboratorio de
   investigación Arco, del que formo parte.
   Todo lo que se puede obtener del repositorio ha sido desarrollado utilizando
   ÚNICA Y EXCLUSIVAMENTE software libre.

   Este proyecto es un Proyecto Final de Carrera que ha sido pensado
   inicialmente para el Laboratorio de investigación ArCo de la Universidad
   de Castilla la Mancha, donde se tiene un repositorio[1] de paquetes Debian
   que aloja paquetes Debian que se desarrollan dentro del grupo para ayudar
   en la labor de los trabajadores. El repositorio es público y sirve
   paquetes no oficiales para varias arquitecturas. En el laboratorio de
   investigación utilizamos en todos los ordenadores Debian GNU/Linux para
   desarrollar nuestro trabajo diario.


   Existe un blog[2] con las novedades del proyecto donde también se explica por
   encima que es Zeroc ICE[3] y, de forma resumida algunas de las decisiones
   que se han tomado. Se ha procurado traducir a inglés la mayoría de los
   post por los lectores extranjeros. Recomiendo la lectura del blog
 
   También existe una cuenta de twitter[4] @distdeb donde a través del
   hashtag #preguntadistdeb se resolvieron algunas preguntas que pueden
   surgir sobre el proyecto y las cuales se recomienda su lectura.

   Existe una demo[8] del proyecto donde se inician las máquinas virtuales y
   se construyen dos paquetes "sl" y "gentoo". En la demo se utilizan 3
   nodos, el que tiene instaladas las máquinas virtuales y otros dos nodos,
   cada uno de ellos envía un paquete para ser construido, estos paquetes son
   "sl" y "gentoo". Todos ellos se ejecutan desde el mismo ordenador para
   facilitar el proceso La demo se puede encontrar ver en:
   http://distdeb.wordpress.com/2013/04/19/demo-del-proyecto/






[1] Repositorio Debian de ARCO:
               deb http://babel.esi.uclm.es/arco sid main
               deb-src http://babel.esi.uclm.es/arco sid main

[2] http://distdeb.wordpress.com/
[3] http://www.zeroc.com/ice.html
[4] https://twitter.com/distdeb
[5] http://es.linkedin.com/pub/jos%C3%A9-luis-sanroma-tato/28/27/560
[6] https://bitbucket.org/arco_group/arco-pfc
[7] http://www.debian.org/doc/debian-policy/
[8] http://distdeb.wordpress.com/2013/04/19/demo-del-proyecto/


+INFO sobre el laboratorio ArCo http://webpub.esi.uclm.es/investigacion/grupos/arco