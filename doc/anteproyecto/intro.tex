\section{Introducción}
\label{sec:intro}

Desde hace algunos años, empresas, universidades, grupos de investigación, o
similares, que se dedican al desarrollo de software desarrollan sus propios 
paquetes que posteriormente son usados por sus propios trabajadores o
clientes. Estos paquetes de software como todo software, necesita ser servido,
mantenido y actualizado de alguna forma, para ello se usan repositorios. En
el caso del Software Libre, y en el caso que se va a presentar, una de las
opciones es utilizar repositorios no oficiales de Debian~\cite{debian} que
gestionan paquetes Debian. Estos repositorios no oficiales pueden crearse con
la ayuda de \emph{reprepro}~\cite{reprepro}, que permite al usuario crear un
repositorio personal con todas las características de uno oficial. 

En lugares como el Laboratorio de
investigación ARCO de la Escuela Superior de Informática de la Universidad de
Castilla-La Mancha, existen computadores de diferentes
arquitecturas debido a la renovación que van sufriendo los equipos con el
paso del tiempo. Por ello, un problema típico que existe es cuando se compila
un paquete, por ejemplo con arquitectura `amd64', y se sube al repositorio,
sucederá que a este paquete solo podrán acceder los usuarios de `amd64'. Si
un usuario de `i386' quiere acceder a ese paquete, tiene que conseguir el
código fuente, construirlo y volver a subir el paquete al repositorio. Por lo
tanto, es un requisito contar con al menos un computador con esa arquitectura
que pueda utilizarse para compilar el paquete y que ese usuario, o alguien
que sepa hacerlo, se dedique a compilarlo usando ese computador u otro con la
misma arquitectura. Con lo cual existe una necesidad, y es tener una versión
de los paquetes compatible con cada una de las arquitecturas soportadas en
el laboratorio para que pueda ser usada por lo trabajadores.


Aunque lo ideal sería tener una infraestructura dedicada con al menos un
computador de cada una de las arquitecturas soportadas, en pequeños
entornos de trabajo como el laboratorio ARCO, llegamos al primer problema, y
es que existen recursos limitados y esto no es posible debido a que:


\begin{itemize}
\item No se disponga de computadores con el propósito de estar disponibles y
  dedicados  para compilar paquetes.
\item Los computadores con los que se cuenta podrían no estar siempre
  disponibles cuando se necesiten para realizar la compilación, bien por
  estar apagados, bien por estar siendo usados por los trabajadores o
  cualquier otra circunstancia. Por lo tanto no se puede saber en que
  instante va a haber un computador disponible.
\end{itemize}


Pero además de estas características, pueden darse unas necesidades como por
ejemplo:


\begin{itemize}

\item Según la \emph{Debian Policy}~\cite{policy} un paquete se puede subir siempre
  y cuando existan todas sus dependencias. En este caso puede
  haber otras necesidades como por ejemplo, que se pueda subir el paquete si
  se encuentran las dependencias en ese o en otros repositorios.

\item Puede haber otros requisitos de calidad totalmente distintos a los de
  Debian, como por ejemplo, garantizar que se haya utilizado el mismo
  compilador.

\item A la misma vez pueden surgir otras características dependiendo del
  laboratorio o empresa que no se contemplan en Debian.
\end{itemize}



\subsection{Construcción de paquetes Debian}
\label{sec:paquetes}

%Creación de un paquete
Para ayudar a crear un paquete Debian a parte de documentarse (Contrato
Social de Debian\footnote{\url{http://www.Debian.org/social_contract}}, Guía
del nuevo desarrollador de Debian~\cite{maintainer} y la política de
Debian~\cite{policy}), la propia Debian provee de algunas herramientas para
ayudar en el proceso de construcción de paquetes: 
\begin{itemize}

  % debhelper ~\cite{debhelper}
  \item \textbf{debhelper}~\cite{debhelper}, un framework para la generación de
    paquetes Debian que está formado por un conjunto de scripts.
  

  % svn-buildpackage ~\cite{svn-buildpackage} 
  \item \textbf{svn-buildpackage}~\cite{svn-buildpackage}, permite mantener
    un paquete alojado en un repositorio subversion, proporciona métodos de
    construcción sencillos.


  % pbuilder ~\cite{pbuilder}
  \item \textbf{pbuilder}~\cite{pbuilder} construye los paquetes sobre sistemas base,
  lo que garantiza que las dependencias se cumplan.


  % dh_make ~\cite{dhmake}
  \item \textbf{dhmake}~\cite{dhmake},  se utiliza para crear el directorio Debian
  inicial. Genera plantillas de los archivos que son obligatorios u
  opcionales en el paquete.
  
\end{itemize}

Una vez generado el paquete con éxito se debe comprobar su funcionamiento y
someterlo a una serie de pruebas, para ello existen herramientas como
\emph{lintian}~\cite{lintian} o \emph{piuparts}~\cite{piuparts}.




Debido a la necesidad de simplificar la construcción de paquetes, \emph{Luca
  Falavigna} creó \textbf{deb-o-matic}~\cite{debomatic}. Una utilidad basada
en \emph{pbuilder}~\cite{pbuilder} para crear paquetes fuente de Debian de
forma casi automática y con poca interacción con el usuario sin perder mucho
tiempo con la configuración de software. Esta herramienta también es
ampliable con módulos (como \emph{lintian}) que se cargan y se ejecutan.


Una vez que se ha construido un paquete fuente y se ha probado se debe subir
al repositorio para compartirlo con el resto de usuarios. Como se verá en el
siguiente punto, en Debian existe la \emph{Debian Autobuilder Network} (Sección
\ref{sec:autobuilder}) que es donde se gestiona la recompilación de paquetes
para las arquitecturas que soporta Debian actualmente.




\subsection{Debian Autobuilder Network}
\label{sec:autobuilder}

\href{http://www.Debian.org/devel/buildd/index.en.html}{Autobuild Network} es
la infraestructura dedicada de Debian que se encarga tanto de la compilación
de paquetes para distintas arquitecturas como de la distribución de los
paquetes a cada uno de los repositorios oficiales de las distintas
arquitecturas soportadas por la distribución. Está compuesta por varias
máquinas que utilizan \emph{buildd} para recoger los paquetes del archivo de
Debian y reconstruirlos.
 
Dentro de esta infraestructura se encuentran las siguientes herramientas:
\begin{itemize}
\item
  \textbf{\href{http://www.Debian.org/devel/buildd/wanna-build-states}{Wanna-build}},
  una herramienta que ayuda a coordinar
  la (re)constucción de paquetes a través de una base de datos que mantiene
  una lista con los paquetes  y su estado. Hay una base de datos central por
  cada una de las arquitecturas.
\item \textbf{buildd}, demonio que comprueba periódicamente la base de datos
  y llama a sbuild para construir los paquetes.
\item \textbf{sbuild}, un programa que se encarga de compilar los paquetes en
  entornos  enjaulados aislados.
\item \textbf{quinn-diff}, que compara las versiones disponibles en dos
  aqruitecturas y muestra sus diferencias.
\end{itemize}


Esta infraestructura funciona muy bien en Debian porque tienen máquinas dedicadas
única y exclusivamente a la compilación de paquetes para servirlos en los
repositorios oficiales, siguiendo la política de Debian~\cite{policy}, y
además, tienen unas herramientas como las listadas anteriormente hechas por y
para la infraestructura de Debian. Pero esta infraestructura es difícil de
configurar,  excede las necesidades de los pequeños entornos (comentadas al
principio de esta Sección) y además estos, requieren de otras necesidades
distintas que no se contemplan aquí debido a que no se disponen de los mismos
recursos o porque la política de la empresa tiene otras necesidades. Por lo
tanto, en nuestro caso \emph{buildd} no cumple con nuestras necesidades y no
nos sirve. Se necesita otra infraestructura que si cumpla con las
necesidades de ARCO y es por lo que se propone este proyecto.






% Local Variables:
%   coding: utf-8
%   fill-column: 90
%   mode: flyspell
%   ispell-local-dictionary: "castellano"
%   mode: latex
%   TeX-master: "main"
% End:
