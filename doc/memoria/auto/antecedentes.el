(TeX-add-style-hook "antecedentes"
 (lambda ()
    (LaTeX-add-labels
     "chap:antecedentes"
     "fig:informatica"
     "sec:uncuadro"
     "tab:rpc-semantics"
     "sec:listado"
     "sec:paginas")
    (TeX-run-style-hooks
     "latex2e"
     "arco-pfc10"
     "arco-pfc"
     "twoside"
     "tables/RPC-semantics")))

