(TeX-add-style-hook "main"
 (lambda ()
    (LaTeX-add-bibliographies)
    (TeX-run-style-hooks
     "custom"
     "latex2e"
     "arco-pfc10"
     "arco-pfc"
     "metadata"
     "resumen"
     "acro"
     "thanks"
     "intro"
     "antecedentes"
     "objetivos"
     "apendice1")))

